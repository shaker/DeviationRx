#define PKTS_PER_CHANNEL 4
#define NUM_WAIT_LOOPS (100 / 5)
#define CHAN_MULTIPLIER 100
#define CHAN_MAX_VALUE (100 * CHAN_MULTIPLIER)
#define NUM_OUT_CHANNELS 12
#define BIND_COUNT 0x1388
enum PktState {
	DEVO_BIND=1,
	DEVO_BIND_SENDCH=2,
	DEVO_BOUND=3,
	DEVO_BOUND_1=4,
	DEVO_BOUND_2=5,
	DEVO_BOUND_3=6,
	DEVO_BOUND_4=7,
	DEVO_BOUND_5=8,
	DEVO_BOUND_6=9,
	DEVO_BOUND_7=10,
	DEVO_BOUND_8=11,
	DEVO_BOUND_9=12,
	DEVO_BOUND_10=13,
};
byte num_channels;
byte packet[16];
int bind_counter;
byte use_fixed_id;
byte pkt_num;
unsigned long fixed_id;
byte txState;
enum PktState state;
byte ch_idx;
int Channels[NUM_OUT_CHANNELS];
byte failsafe_pkt;
byte radio_ch[5];
byte *radio_ch_ptr;
byte cyrfmfg_id[6];

void build_bind_pkt()
{
	packet[0] = (num_channels << 4) | 0x0a;
	packet[1] = bind_counter & 0xff;
	packet[2] = (bind_counter >> 8);
	packet[3] = *radio_ch_ptr;
	packet[4] = *(radio_ch_ptr + 1);
	packet[5] = *(radio_ch_ptr + 2);
	packet[6] = cyrfmfg_id[0];
	packet[7] = cyrfmfg_id[1];
	packet[8] = cyrfmfg_id[2];
	packet[9] = cyrfmfg_id[3];
	add_pkt_suffix();
	//The fixed-id portion is scrambled in the bind packet
	//I assume it is ignored
	packet[13] ^= cyrfmfg_id[0];
	packet[14] ^= cyrfmfg_id[1];
	packet[15] ^= cyrfmfg_id[2];
}

void add_pkt_suffix()
{
	byte bind_state;
	if (use_fixed_id) {
		if (bind_counter > 0) {
			bind_state = 0xc0;
		} else {
			bind_state = 0x80;
		}
	} else {
		bind_state = 0x00;
	}
	packet[10] = bind_state | (PKTS_PER_CHANNEL - pkt_num - 1);
	packet[11] = *(radio_ch_ptr + 1);
	packet[12] = *(radio_ch_ptr + 2);
	packet[13] = fixed_id  & 0xff;
	packet[14] = (fixed_id >> 8) & 0xff;
	packet[15] = (fixed_id >> 16) & 0xff;
}

static void build_data_pkt()
{
	byte i;
	packet[0] = (num_channels << 4) | (0x0b + ch_idx);
	byte sign = 0x0b;
	for (i = 0; i < 4; i++) {
		long value = (long)Channels[ch_idx * 4 + i] * 0x640 / CHAN_MAX_VALUE;
		if(value < 0) {
			value = -value;
			sign |= 1 << (7 - i);
		}
		packet[2 * i + 1] = value & 0xff;
		packet[2 * i + 2] = (value >> 8) & 0xff;
	}
	packet[9] = sign;
	ch_idx = ch_idx + 1;
	if (ch_idx * 4 >= num_channels)
		ch_idx = 0;
	add_pkt_suffix();
}

void cyrf_set_bound_sop_code()
{
	/* crc == 0 isn't allowed, so use 1 if the math results in 0 */
	byte crc = (cyrfmfg_id[0] + (cyrfmfg_id[1] >> 6) + cyrfmfg_id[2]);
	if(! crc)
		crc = 1;
	byte sopidx = (0xff &((cyrfmfg_id[0] << 2) + cyrfmfg_id[1] + cyrfmfg_id[2])) % 10;
	CYRF_ConfigRxTx(1);
	CYRF_ConfigCRCSeed((crc << 8) + crc);
	CYRF_ConfigSOPCode(sopcodes[sopidx]);
	CYRF_WriteRegister(CYRF_03_TX_CFG, 0x08 | /*Model.tx_power*/7);
}

void scramble_pkt()
{
#ifdef NO_SCRAMBLE
	return;
#else
	byte i;
	for(i = 0; i < 15; i++) {
		packet[i + 1] ^= cyrfmfg_id[i % 4];
	}
#endif
}

void build_beacon_pkt(int upper)
{
	packet[0] = ((num_channels << 4) | 0x07);
	byte enable = 0;
	int max = 8;
	int offset = 0;
	if (upper) {
		packet[0] += 1;
		max = 4;
		offset = 8;
	}
	for(int i = 0; i < max; i++) {
		//if (i + offset < Model.num_channels && Model.limits[i+offset].flags & CH_FAILSAFE_EN) {
		//    enable |= 0x80 >> i;
		//    packet[i+1] = Model.limits[i+offset].failsafe;
		//} else {
		packet[i+1] = 0;
		//}
	}
	packet[9] = enable;
	add_pkt_suffix();
}


void set_radio_channels()
{
	int i;
	CYRF_FindBestChannels(radio_ch, 5, 4, 4, 80);
	printf("Radio Channels:");
	for (i = 0; i < 3; i++) {
		printf(" %02x", radio_ch[i]);
	}
	printf("\n");
}

void initialize()
{
	//MsTimer2::stop();
	{
		display.begin(SSD1306_SWITCHCAPVCC);
		//display.display(); // show splashscreen
		//delay(1000);
		display.clearDisplay();   // clears the screen and buffer
		display.setTextSize(1);
		display.setTextColor(WHITE);
		display.setCursor(0,0);
		display.display();
	}
	pinMode(2,OUTPUT);
	pinMode(3,OUTPUT);
	digitalWrite(2,LOW);
	digitalWrite(3,LOW);

	pinMode(CYRF_CS,OUTPUT);
	pinMode(CYRF_RS,OUTPUT);
	CS_HI();
	CYRF_Reset();
	{
		//display.println("setup finished.");
		//display.display();
		//Serial.println("setup finished.");
		byte a=CYRF_ReadRegister(CYRF_0C_XTAL_CTRL);
		display.print("XTAL_CTRL=");
		display.println(a,BIN);
		a=CYRF_ReadRegister(CYRF_0D_IO_CFG);
		display.print("IO_CFG=");
		display.println(a,BIN);
		a=CYRF_ReadRegister(CYRF_0E_GPIO_CTRL);
		display.print("GPIO_CTRL=");
		display.println(a,BIN);
		display.display();
	}
	CYRF_Init();
	CYRF_GetMfgData(cyrfmfg_id);
	{
		display.print("MFG=");
		int i;
		for(i=0;i<5;i++)
		{
			display.print(cyrfmfg_id[i],HEX);
			display.print(" ");
		}
		display.println(cyrfmfg_id[i],HEX);
		display.display();
	}
	CYRF_ConfigRxTx(1);
	CYRF_ConfigCRCSeed(0x0000);
	CYRF_ConfigSOPCode(sopcodes[0]);
	//{
	//	byte old_sop[8];
	//	ReadRegisterMulti(0x22,old_sop,8);
	//	display.print("Sop:");
	//	for(int i=0;i<5;i++)
	//	{
	//		display.print(old_sop[i],HEX);
	//		display.print(" ");
	//	}
	//	display.println("");
	//	display.display();	
	//	byte l,h;
	//	l=CYRF_ReadRegister(0x15);
	//	h=CYRF_ReadRegister(0x16);
	//	display.print("Seed=");
	//	display.print(h,HEX);
	//	display.print(l,HEX);
	//	display.println("");
	//	display.display();
	//}
	set_radio_channels();
	{
		display.print("CHN::");
		display.display();
		for(int i=0;i<3;i++)
		{
			display.print(radio_ch[i],HEX);
			display.print(" ");
		}
		display.println("");
		display.display();
	}
	use_fixed_id = 0;
	failsafe_pkt = 0;
	radio_ch_ptr = radio_ch;
	//memset(&Telemetry, 0, sizeof(Telemetry));
	CYRF_ConfigRFChannel(*radio_ch_ptr);

	num_channels = ((/*Model.num_channels*/8 + 3) >> 2) * 4; //8通道
	pkt_num = 0;
	ch_idx = 0;
	txState = 0;


	//if(! Model.fixed_id)
	{//不固定id
		fixed_id = ((unsigned long)(radio_ch[0] ^ cyrfmfg_id[0] ^ cyrfmfg_id[3]) << 16)
			| ((unsigned long)(radio_ch[1] ^ cyrfmfg_id[1] ^ cyrfmfg_id[4]) << 8)
			| ((unsigned long)(radio_ch[2] ^ cyrfmfg_id[2] ^ cyrfmfg_id[5]) << 0);
		fixed_id = fixed_id % 1000000;
		bind_counter = BIND_COUNT;
		state = DEVO_BIND;
		//PROTOCOL_SetBindState(0x1388 * 2400 / 1000); //TODO:完成PROTOCOL_SetBindState msecs
	}
	//else
	//{
	//	fixed_id = Model.fixed_id;
	//	use_fixed_id = 1;
	//	state = DEVO_BOUND_1;
	//	bind_counter = 0;
	//	cyrf_set_bound_sop_code();
	//}
	//MsTimer2::set(2, devo_cb);
	//MsTimer2::start();
	//if (Model.proto_opts[PROTOOPTS_TELEMETRY] == TELEM_ON) {
	//	CLOCK_StartTimer(2400, devo_telemetry_cb);
	//} else {
	//	CLOCK_StartTimer(2400, devo_cb);
	//}
}

int devo_cb()
{
	if (txState == 0) {
		//Serial.println("send packet");
		txState = 1;
		DEVO_BuildPacket();
		CYRF_WriteDataPacket(packet);
		return 1200;
	}
	txState = 0;
	int i = 0;
	unsigned long time=micros();
	byte IRQ_STATUS=0;
	while (! ((IRQ_STATUS=CYRF_ReadRegister(CYRF_04_TX_IRQ_STATUS)) & 0x02)) {
		//Serial.println(IRQ_STATUS,BIN);
		if((micros()-time)>=100)
		{
			//Serial.println("send packet fail");
			return  1200;
		}
	}
	Serial.print("TX_IRQ_STATUS=");
	Serial.println(IRQ_STATUS,BIN);
	Serial.print("TX_LENGTH=");
	Serial.println(CYRF_ReadRegister(CYRF_01_TX_LENGTH));
	if (state == DEVO_BOUND) {
		/* exit binding state */
		state = DEVO_BOUND_3;
		cyrf_set_bound_sop_code();
	}   
	if(pkt_num == 0) {
		//Keep tx power updated
		CYRF_WriteRegister(CYRF_03_TX_CFG, 0x08 | /*Model.tx_power*/7);
		radio_ch_ptr = radio_ch_ptr == &radio_ch[2] ? radio_ch : radio_ch_ptr + 1;
		CYRF_ConfigRFChannel(*radio_ch_ptr);
	}
	return  1200;
}

void DEVO_BuildPacket()
{
	switch(state) {
	case DEVO_BIND:
		//Serial.println("DEVO_BuildPacket: DEVO_BIND");
		bind_counter--;
		build_bind_pkt();
		state = DEVO_BIND_SENDCH;
		break;
	case DEVO_BIND_SENDCH:
		//Serial.println("DEVO_BuildPacket: DEVO_BIND_SENDCH");
		bind_counter--;
		build_data_pkt();
		scramble_pkt();
		if (bind_counter <= 0) {
			state = DEVO_BOUND;
			//TOTO:PROTOCOL_SetBindState(0);
		} else {
			state = DEVO_BIND;
		}
		break;
	case DEVO_BOUND:
	case DEVO_BOUND_1:
	case DEVO_BOUND_2:
	case DEVO_BOUND_3:
	case DEVO_BOUND_4:
	case DEVO_BOUND_5:
	case DEVO_BOUND_6:
	case DEVO_BOUND_7:
	case DEVO_BOUND_8:
	case DEVO_BOUND_9:
		//Serial.println("DEVO_BuildPacket: DEVO_BOUND");
		build_data_pkt();
		scramble_pkt();
		//state++;
		if (bind_counter > 0) {
			bind_counter--;
			//if (bind_counter == 0)
			//TOTO:PROTOCOL_SetBindState(0);
		}
		break;
	case DEVO_BOUND_10:
		//Serial.println("DEVO_BuildPacket: DEVO_BOUND_10");
		build_beacon_pkt(num_channels > 8 ? failsafe_pkt : 0);
		failsafe_pkt = failsafe_pkt ? 0 : 1;
		scramble_pkt();
		state = DEVO_BOUND_1;
		break;
	}
	pkt_num++;
	if(pkt_num == PKTS_PER_CHANNEL)
		pkt_num = 0;
}
